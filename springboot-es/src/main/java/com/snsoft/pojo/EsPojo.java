package com.snsoft.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EsPojo {

    private long id;
    private String name;
    private Integer age;
    private String sex;
    private String desc;

}
