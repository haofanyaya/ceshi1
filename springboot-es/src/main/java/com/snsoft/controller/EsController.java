package com.snsoft.controller;

import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class EsController {

    @Autowired
    private TransportClient transportClient;

    private static TransportClient client;

    /**
     * @PostContruct是spring框架的注解 spring容器初始化的时候执行该方法
     */
    @PostConstruct
    public void init() {
        client = this.transportClient;
    }


    @RequestMapping("search")
    public List<Map<String, Object>> search(String name) {
        SearchRequestBuilder searchRequestBuilder = client.prepareSearch("testdoct");
        searchRequestBuilder.setTypes("testbean");
        searchRequestBuilder.addSort("id", SortOrder.DESC);
        BoolQueryBuilder builder = QueryBuilders.boolQuery();
        MatchQueryBuilder matchQuery = QueryBuilders.matchQuery("desc", name);
        builder.must(matchQuery);
        searchRequestBuilder.setQuery(builder);
        SearchResponse searchResponse = searchRequestBuilder.execute().actionGet();
        List<Map<String, Object>> sourceList = new ArrayList<Map<String, Object>>();
        if (searchResponse.status().getStatus() == 200) {
            for (SearchHit searchHit : searchResponse.getHits().getHits()) {
                sourceList.add(searchHit.getSourceAsMap());
            }

            return sourceList;
        }
        return null;
    }
}
